//
//  AppDelegate.h
//  Pokemon
//
//  Created by Jordan Schoen on 7/11/16.
//  Copyright © 2016 Jordan Schoen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

